<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Febri Daniswara</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="biodata.html">Biodata</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="berita.html">Berita</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="galeri.html">Galeri</a>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>

    <div class="row">
        <div class="col-md-3 p-3">
            <div class="card">
                <div class="card-body">
                    <img src="Foto.jpg" class="img-fluid" alt="TUGAS PERTEMUAN 4">
                </div>   
            </div>
        </div>
        <div class="col-md-9 p-3">
            <div class="card">
                <div class="card-body bg-primary">
                    <table class="table table-hover table primary">
                        <thead>
                            <h1 class="text-center"><i>Biodata Diri</i></h1>
                        </thead>
                        <tbody>
                            <tr class="table-success">
                                <td>NPM</td>
                                <td>:</td>
                                <td>011220041</td>
                            </tr>
                            <tr class="table-success">
                                <td>Nama</td>
                                <td>:</td>
                                <td>Febri Daniswara</td> 
                            </tr>
                            <tr class="table-success">
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>Pria</td> 
                            </tr>
                            <tr class="table-success">
                                <td>Tempat Tanggal Lahir</td>
                                <td>:</td>
                                <td>Palembang, 17 Februari 2003</td> 
                            </tr>
                            <tr class="table-success">
                                <td>Alamat</td>
                                <td>:</td>
                                <td>Jl. Irigasi Lr. Mangin Angin No 3298 RT. 054 RW. 015 Kel. Srijaya Kec. Alang-Alang Lebar Palembang</td>
                            </tr>
                            <tr class="table-success">
                                <td>No HP</td>
                                <td>:</td>
                                <td>0895365296785</td>
                            </tr>  
                            <tr class="table-success">
                                <td>Hobi</td>
                                <td>:</td>
                                <td>
                                    <ul>
                                        <li>Olahraga</li>
                                        <li>Bermain Game</li>
                                        <li>Desain Grafis</li>
                                    </ul>
                                </td>
                            </tr> 
                            <tr class="table-success">
                                <td>Riwayat Pendidikan</td>
                                <td>:</td>
                                <td>
                                    <ol>
                                        <li>SD Negeri 26 Palembang</li>
                                        <li>SMP Negeri 22 Palembang</li>
                                        <li>SMA Negeri 11 Palembang</li>
                                    </ol>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>